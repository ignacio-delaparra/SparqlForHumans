﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SparqlForHumans.Core.Properties
{
    //Rename: Name -> ID
    //Rename: Type -> InstanceOf
    //Rename: PO -> ??
    public enum Labels
    {
        Label,
        Property,
        Name,
        Type,
        AltLabel,
        Description,
        PO,
    }
}
